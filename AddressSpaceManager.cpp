#include "AddressSpaceManager.h"
#include <iostream>

const PageNum AddressSpaceManager::VM_SPACE_SIZE = 1UL << 24;

AddressSpaceManager::AddressSpaceManager() {

}

int AddressSpaceManager::allocateSegment(VirtualAddress startAddress, PageNum segSize) {
	// izlazi van dozvoljenog opsega
	if ((startAddress < 0) || (startAddress + segSize * PAGE_SIZE > VM_SPACE_SIZE))
		return -1;

	// pocetna adresa nije poravnata na pocetak stranice
	if (startAddress & 0x000003FF)
		return -2;

	// nepotrebno? vrv
	if (segments.empty()) {
		segments.push_back(Segment(startAddress, segSize));
		return 0;
	}

	// proverava preklapanje sa nekim od vec alociranih segmenata
	for (auto it = segments.begin(); it != segments.end(); it++)
		if ((startAddress < it->first + it->second * PAGE_SIZE) && (it->first < startAddress + segSize * PAGE_SIZE))
			return -3;

	segments.push_back(Segment(startAddress, segSize));
	return 0;
}

int AddressSpaceManager::deallocateSegment(VirtualAddress startAddress) {
	for (auto it = segments.begin(); it != segments.end(); it++)
		if (it->first == startAddress) {
			segments.erase(it);
			return 0;
		}
	return -1;
}

PageNum AddressSpaceManager::getSegmentSize(VirtualAddress startAddress) const {
	for (auto it = segments.begin(); it != segments.end(); it++)
		if (it->first == startAddress)
			return it->second;
	return 0;
}

std::ostream& operator<<(std::ostream& os, const AddressSpaceManager& adrSM) {
	os << "Allocated address space segments:" << std::endl;
	if (adrSM.segments.empty())
		os << "No allocated segments" << std::endl;
	else for (auto it = adrSM.segments.begin(); it != adrSM.segments.end(); it++)
		os << it->first << ":" << it->second << " ";
	os << std::endl;
	return os;
}