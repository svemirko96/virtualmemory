#include "KernelSystem.h"
#include "Process.h"
#include "KernelProcess.h"
#include "part.h"
#include "vm_utils.h"
#include <iostream>

ProcessId KernelSystem::nextProcessID = 0;

KernelSystem::KernelSystem(System* system, PhysicalAddress processVMSpace, PageNum processVMSpaceSize, PhysicalAddress pmtSpace, PageNum pmtSpaceSize, Partition* partition) :
pvmSM(processVMSpace, processVMSpaceSize),
pmtSM(pmtSpace, pmtSpaceSize),
partSM(0, partition->getNumOfClusters()) {
	this->system = system;
	this->processVMSpace = processVMSpace;
	this->processVMSpaceSize = processVMSpaceSize;
	this->pmtSpace = pmtSpace;
	this->pmtSpaceSize = pmtSpaceSize;
	this->partition = partition;
}

KernelSystem::~KernelSystem() {
	for (KernelProcess* pProc : processes)
		delete pProc;
}

Process* KernelSystem::createProcess() {
	std::lock_guard<std::mutex> lock(mtx);

	Process* proc = new Process(nextProcessID++);
	processes.push_back(proc->pProcess);

	proc->pProcess->pSystem = this;

	// initialize allocated pmt by setting all bits to 0
	PhysicalAddress* pmt = (PhysicalAddress*)(pmtSM.allocateBlock() << 10);
	proc->pProcess->pmt = pmt;

	for (int i = 0; i < PAGE_SIZE / sizeof(PageDescriptor); i++)
		pmt[i] = nullptr;

	return proc;
}

Time KernelSystem::periodicJob() {
	return 0;
}

PageNum KernelSystem::allocatePMTFrame() {
	std::lock_guard<std::mutex> lock(mtx);
	return pmtSM.allocateBlock();
}

int KernelSystem::deallocatePMTFrame(PageNum frameNum) {
	std::lock_guard<std::mutex> lock(mtx);
	return pmtSM.deallocateBlock(frameNum);
}

Status KernelSystem::access(ProcessId pid, VirtualAddress address, AccessType type) {
	std::lock_guard<std::mutex> lock(mtx);

	// trazi se proces ciji je pid jednak prosledjenom pid-u (za sad nije O(1), optimizovati kasnije)
	KernelProcess* proc = nullptr;
	for (auto it = processes.begin(); it != processes.end(); it++) {
		if (pid == (*it)->getProcessId()) {
			proc = (*it);
			break;
		}
	}

	// nije pronadjen proces - greska
	if (!proc) {
		std::cout << "KernelSystem::access: Process not found" << std::endl;
		return TRAP;
	}

	PageNum pmt1Entry = getLvl1PmtEntry(address);
	PageNum pmt2Entry = getLvl2PmtEntry(address);

	// provera da li adresa kojoj se pristupa pripada nekom segmentu
	if (proc->pmt[pmt1Entry] == nullptr) {
		std::cout << "KernelSystem::access: Table for the page not allocated" << std::endl;
		return TRAP;
	}
	
	PageDescriptor* pDesc = (PageDescriptor*)proc->pmt[pmt1Entry] + pmt2Entry;
	PageDescriptor desc = *pDesc;
	
	if (desc == 0) {
		std::cout << "KernelSystem::access: Descriptor for the page not created" << std::endl;
		return TRAP;
	}

	// provera da li je dozvoljen pristup
	switch (type) {
	case READ:
		if (!isFlagSet(desc, READ_BIT))
			return TRAP;
		break;
	case WRITE:
		if (!isFlagSet(desc, WRITE_BIT))
			return TRAP;
		break;
	case READ_WRITE:
		if (!isFlagSet(desc, READ_BIT) || !isFlagSet(desc, WRITE_BIT))
			return TRAP;
		break;
	case EXECUTE:
		if (!isFlagSet(desc, EXEC_BIT))
			return TRAP;
		break;
	default:
		return TRAP;
		break;
	}

	if (!isFlagSet(desc, VALID_BIT))
		return PAGE_FAULT;

	// postavi REF_BIT deskriptora na 1
	setFlag(pDesc, REF_BIT, true);
	// ako je pristup tipa WRITE ili READ_WRITE postavljanje DIRTY_BIT na 1
	if (type == WRITE || type == READ_WRITE)
		setFlag(pDesc, DIRTY_BIT, true);

	return OK;
}

// --------------------------- HELPERS -------------------------------

PageNum KernelSystem::allocateFrame(PageDescriptor* pDesc) {
	std::lock_guard<std::mutex> lock(mtx);

	PageNum frameNum = pvmSM.allocateBlock();

	if (frameNum != INVALID_BLOCK) { // ima slobodnih frejmova, nema potrebe za zamenom vec ucitanih
		loadedPageDescriptors.push_back(pDesc);
		return frameNum;
	}

	// ------------ PAGE SWAP ---------------
	bool ref;
	while(1) {
		PageDescriptor* pOutDesc = loadedPageDescriptors.front();
		loadedPageDescriptors.erase(loadedPageDescriptors.begin());
		ref = isFlagSet(*pOutDesc, REF_BIT);

		// REF_BIT = 1, postavlja se na 0 i vraca na kraj reda (pruza se jos jedna sansa)
		if (ref) {
			setFlag(pOutDesc, REF_BIT, false);
			loadedPageDescriptors.push_back(pOutDesc);
			continue;
		}
		
		// REF_BIT = 0, cuvanje na disk ako je stranica zaprljana
		frameNum = getFrameNum(*pOutDesc);

		if (isFlagSet(*pOutDesc, DIRTY_BIT)) {
			PageNum clusterNum = getClusterNum(*pOutDesc);
			char* buff = (char*)(frameNum << 14);
			partition->writeCluster(clusterNum, buff);
		}

		// azuriranje deskriptora izbacene stranice
		setFlag(pOutDesc, REF_BIT, false);
		setFlag(pOutDesc, VALID_BIT, false);
		
		loadedPageDescriptors.push_back(pDesc);
		break;
	}

	return frameNum;
}

int KernelSystem::deallocateFrame(PageNum frameNum) {
	std::lock_guard<std::mutex> lock(mtx);

	int ret = pvmSM.deallocateBlock(frameNum);
	if (ret) // greska
		return ret;

	for (int i = 0; i < loadedPageDescriptors.size(); i++) {
		if (frameNum == getFrameNum(*loadedPageDescriptors[i])) {
			loadedPageDescriptors.erase(loadedPageDescriptors.begin() + i);
			break;
		}
	}

	return 0;
}

PageNum KernelSystem::allocateCluster() {
	std::lock_guard<std::mutex> lock(mtx);

	return partSM.allocateBlock();
}

int KernelSystem::deallocateCluster(PageNum clusterNum) {
	std::lock_guard<std::mutex> lock(mtx);

	return partSM.deallocateBlock(clusterNum);
}