#include "vm_utils.h"

PageNum getLvl1PmtEntry(VirtualAddress address) {
	return address >> 17;
}

PageNum getLvl2PmtEntry(VirtualAddress address) {
	return (address >> 10) & 0x7F;
}

PageNum getOffset(VirtualAddress address) {
	return address & 0x3FF;
}

bool isFlagSet(PageDescriptor desc, PageDescFlag flag) {
	switch (flag) {
	case REF_BIT:
		return desc & 0x8000000000000000;
	case VALID_BIT:
		return desc & 0x4000000000000000;
	case DIRTY_BIT:
		return desc & 0x2000000000000000;
	case READ_BIT:
		return desc & 0x1000000000000000;
	case WRITE_BIT:
		return desc & 0x0800000000000000;
	case EXEC_BIT:
		return desc & 0x0400000000000000;
	default:
		return false;
	}
}

void setFlag(PageDescriptor* desc, PageDescFlag flag, bool val) {
	unsigned long long mask = 0;
	switch (flag) {
	case REF_BIT:
		mask = 0x8000000000000000;
		break;
	case VALID_BIT:
		mask = 0x4000000000000000;
		break;
	case DIRTY_BIT:
		mask = 0x2000000000000000;
		break;
	case READ_BIT:
		mask = 0x1000000000000000;
		break;
	case WRITE_BIT:
		mask = 0x0800000000000000;
		break;
	case EXEC_BIT:
		mask = 0x0400000000000000;
		break;
	default:
		return;
	}

	if (val) {
		*desc |= mask;
	}
	else {
		*desc &= ~mask;
	}
}

AccessType getAccesType(PageDescriptor desc) {
	if (isFlagSet(desc, READ_BIT)) {
		if (isFlagSet(desc, WRITE_BIT))
			return READ_WRITE;
		else
			return READ;
	}
	else if (isFlagSet(desc, WRITE_BIT))
		return WRITE;
	else
		return EXECUTE;
}

PageNum getFrameNum(PageDescriptor desc) {
	return (desc >> 14) & 0x3FFFFF;
}

void setFrameNum(PageDescriptor* pDesc, PageNum frameNum) {
	*pDesc = (*pDesc & ~(0x3FFFFFULL << 14)) | (frameNum << 14);
}

PageNum getClusterNum(PageDescriptor desc) {
	return desc & 0x3FFF;
}

void setClusterNum(PageDescriptor* pDesc, PageNum clusterNum) {
	*pDesc = (*pDesc & ~0x3FFFULL) | clusterNum;
}