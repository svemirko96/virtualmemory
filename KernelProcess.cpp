#include "KernelProcess.h"
#include "KernelSystem.h"
#include "part.h"
#include "vm_utils.h"

// debug
#include <iostream>
#include <cstdio>

KernelProcess::KernelProcess(Process* proc, ProcessId pid) {
	this->pProcess = proc;
	this->pid = pid;
}

KernelProcess::~KernelProcess() {
	// Oslobadjanje memorije koju zauzima PMT
	// Tabele drugog reda se same dealociraju brisanjem svih segmenata
	for (auto segment : adrSM.segments)
		deleteSegment(segment.first);
	PageNum frameNum = (PageNum)pmt >> 10;
	pSystem->deallocatePMTFrame(frameNum);
}

ProcessId KernelProcess::getProcessId() const {
	return this->pid;
}

Status KernelProcess::createSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags) {
	return loadSegment(startAddress, segmentSize, flags, nullptr);
}

Status KernelProcess::loadSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags, void* content) {
	
	int retVal = 0;
	if ((retVal = adrSM.allocateSegment(startAddress, segmentSize)) != 0) {
		std::cout << "KernelProcess::createSegment(): error while allocating address segment; error code " << retVal << std::endl;
		return TRAP;
	}

	for (PageNum pageNum = 0; pageNum < segmentSize; pageNum++) {
		VirtualAddress pageAddress = startAddress + pageNum * PAGE_SIZE;
		PageNum pmt1Entry = getLvl1PmtEntry(pageAddress);
		PageNum pmt2Entry = getLvl2PmtEntry(pageAddress);

		// Alociranje tabele drugog nivoa u ulazu pm1Entry
		if (!pmt[pmt1Entry]) {
			PageNum frameNum = pSystem->allocatePMTFrame();
			if (frameNum == INVALID_BLOCK)
				return TRAP;
			pmt[pmt1Entry] = (PhysicalAddress)(frameNum << 10);
			for (int i = 0; i < 128; i++)
				*((PageDescriptor*)pmt[pmt1Entry] + i) = 0; // inicijalizacija na pocetnu vrednost svih deskriptora alocirane tabele drugog nivoa
		}

		PageDescriptor desc = *((PageDescriptor*)pmt[pmt1Entry] + pmt2Entry) = generatePageDescriptor(flags);
		
		if (content)
			pSystem->partition->writeCluster(getClusterNum(desc), (char*)content + pageNum * PAGE_SIZE);
	}

	return OK;
}

Status KernelProcess::deleteSegment(VirtualAddress startAddress) {
	PageNum segmentSize = adrSM.getSegmentSize(startAddress);
	if (segmentSize == 0) // Nije pronadjen segment sa datom pocetnom adresom
		return TRAP;

	for (PageNum pageNum = 0; pageNum < segmentSize; pageNum++) {
		VirtualAddress pageAddress = startAddress + pageNum * PAGE_SIZE;
		PageNum pmt1Entry = getLvl1PmtEntry(pageAddress);
		PageNum pmt2Entry = getLvl2PmtEntry(pageAddress);

		PageDescriptor* pDesc = (PageDescriptor*)pmt[pmt1Entry] + pmt2Entry;

		// Oslobadjanje okvira u memoriji ako je data stranica ucitana u memoriju i brisanje deskriptora iz evidencije
		if (isFlagSet(*pDesc, VALID_BIT))
			pSystem->deallocateFrame(getFrameNum(*pDesc));
		
		// Oslobadjanje klastera particije
		pSystem->deallocateCluster(getClusterNum(*pDesc));

		// Oznacavanje ulaza slobodnim unutar tabele drugog reda
		*pDesc = 0;

		// Provera ako smo oslobodili sve ulaze tabele, dealociramo prostor za tabelu - NEOPTIMIZOVANO (proverava uvek)
		bool dealloc = true;
		for (int i = 0; i < 128; i++) {
			if (*((PageDescriptor*)pmt[pmt1Entry] + i) != 0) {
				dealloc = false;
				break;
			}
		}
		if (dealloc) {
			PageNum frameNum = (PageNum)pmt[pmt1Entry] >> 10;
			pSystem->deallocatePMTFrame(frameNum);
			pmt[pmt1Entry] = nullptr;
		}
	}

	adrSM.deallocateSegment(startAddress);

	return OK;
}

Status KernelProcess::pageFault(VirtualAddress address) {
	PageNum pmt1Entry = getLvl1PmtEntry(address);
	PageNum pmt2Entry = getLvl2PmtEntry(address);

	PageDescriptor* pDesc = (PageDescriptor*)pmt[pmt1Entry] + pmt2Entry;
	PageNum clusterNum = getClusterNum(*pDesc);

	// system->allocateFrame() radi page swap po potrebi
	PageNum allocatedFrameNum = pSystem->allocateFrame(pDesc);

	// Ucitavanje sadrzaja sa klastera u alocirani(preoteti, u slucaju page-swap) okvir
	pSystem->partition->readCluster(clusterNum, (char*)(allocatedFrameNum << 10));

	// azuriranje broja alociranog frejma i bita validnosti u deskriptoru stranice
	setFrameNum(pDesc, allocatedFrameNum);
	setFlag(pDesc, VALID_BIT, true);

	// ZABORAVIO
	setFlag(pDesc, DIRTY_BIT, false);

	return OK;
}

PhysicalAddress KernelProcess::getPhysicalAddress(VirtualAddress address) {
	PageNum pmt1Entry = getLvl1PmtEntry(address);
	PageNum pmt2Entry = getLvl2PmtEntry(address);
	PageNum offset = getOffset(address);

	if (pmt[pmt1Entry] == nullptr)
		return nullptr;

	PageDescriptor desc = ((PageDescriptor*)pmt[pmt1Entry])[pmt2Entry];
	
	if (desc == 0)
		return nullptr;

	return (PhysicalAddress)((getFrameNum(desc) << 10) + offset);
}

// --------------------------- HELPERS ---------------------------

PageDescriptor KernelProcess::generatePageDescriptor(AccessType flags) {
	PageNum clusterNum = pSystem->allocateCluster();
	if (clusterNum == INVALID_BLOCK)
		return 0;

	PageDescriptor desc = 0;

	// zameniti kasnije pomocnim funkcijama setFlag() i setClusterNum()
	switch (flags) {
	case READ:
		desc |= (0x1000000000000000) + clusterNum;
		break;
	case WRITE:
		desc |= (0x0800000000000000) + clusterNum;
		break;
	case READ_WRITE:
		desc |= (0x1800000000000000) + clusterNum;
		break;
	case EXECUTE:
		desc |= (0x0400000000000000) + clusterNum;
		break;
	default:
		return 0;
		break;
	}

	return desc;
}