#include "BlockSpaceManager.h"
#include <iostream>

BlockSpaceManager::BlockSpaceManager(PhysicalAddress startAddress, PageNum size):
firstBlockNum((PageNum)startAddress >> 10),
size(size) {
	freeBlocks.push_back(std::pair<PageNum, PageNum>((PageNum)startAddress >> 10, size));
}

PageNum BlockSpaceManager::allocateBlock() {
	if (freeBlocks.empty())
		return INVALID_BLOCK;
	PageNum blockNum = freeBlocks[0].first++;
	if (!(--freeBlocks[0].second))
		freeBlocks.erase(freeBlocks.begin());
	return blockNum;
}

int BlockSpaceManager::deallocateBlock(PageNum block) {
	if (block < firstBlockNum || block >= (firstBlockNum + size))
		return -1;

	if (freeBlocks.empty()) {
		freeBlocks.push_back(std::pair<PageNum, PageNum>(block, 1));
		return 0;
	}

	for (auto it = freeBlocks.begin(); it != freeBlocks.end(); it++) {
		if (block < it->first) {
			if (block == it->first - 1) {
				--it->first;
				++it->second;
			}
			else {
				freeBlocks.insert(it, std::pair<PageNum, PageNum>(block, 1));
			}
			return 0;
		}
		if (it + 1 != freeBlocks.end() && block >= it->first + it->second && block < (it + 1)->first) {
			if (block == it->first + it->second) {
				if (block < (it + 1)->first - 1) {
					++it->second;
				}
				else {
					(it + 1)->first -= it->second + 1;
					(it + 1)->second += it->second + 1;
					freeBlocks.erase(it);
				}
			}
			else if (block == (it + 1)->first - 1) {
				--(it + 1)->first;
				++(it + 1)->second;
			}
			else {
				freeBlocks.insert(it + 1, std::pair<PageNum, PageNum>(block, 1));
			}
			return 0;
		}
	}
	if (block >= freeBlocks.back().first + freeBlocks.back().second) {
		if (block == freeBlocks.back().first + freeBlocks.back().second) {
			++freeBlocks.back().second;
		}
		else {
			freeBlocks.push_back(std::pair<PageNum, PageNum>(block, 1));
		}
	}

	return 0;
}

std::ostream& operator<<(std::ostream& os, const BlockSpaceManager& bsm) {
	os << "Free blocks:" << std::endl;
	if (bsm.freeBlocks.empty())
		os << "X:0";
	else for (auto it = bsm.freeBlocks.begin(); it != bsm.freeBlocks.end(); it++)
		os << it->first << ":" << it->second << " ";
	os << std::endl;
	return os;
}