#ifndef VM_DECLARATIONS_H
#define VM_DECLARATIONS_H

typedef unsigned long PageNum;
typedef unsigned long VirtualAddress;
typedef void* PhysicalAddress;
typedef unsigned long Time;

enum Status {OK, PAGE_FAULT, TRAP};

enum AccessType {READ, WRITE, READ_WRITE, EXECUTE};

typedef unsigned ProcessId;

#define PAGE_SIZE 1024

// ----------- moje deklaracije ---------------

typedef unsigned long long PageDescriptor;

#define INVALID_BLOCK 0xFFFFFFFF

enum PageDescFlag {REF_BIT, VALID_BIT, DIRTY_BIT, READ_BIT, WRITE_BIT, EXEC_BIT};

#endif