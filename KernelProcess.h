#ifndef KERNEL_PROCESS_H
#define KERNEL_PROCESS_H

#include "vm_declarations.h"
#include "AddressSpaceManager.h"

class Process;

class KernelProcess {
public:
	KernelProcess(Process* proc, ProcessId pid);

	/*
		Oslobadja memoriju koju zauzima PMT prvog nivoa
	*/
	~KernelProcess();

	ProcessId getProcessId() const;

	/*
		1. Oznacava deo trazenog adresnog prostora zauzetim ako je slobodan; u suprotnom vraca TRAP
		2. Alocira potrebne tabele drugog nivoa; ako nema dovoljno blokova u PMTspace vraca TRAP
		3. U ulaze tabele prvog nivoa upisuje pokazivace na alocirane tabele drugog reda
		4. Inicijalizuje deskriptor svake alocirane stranice datog segmenta bitovima prava pristupa i brojem klastera na disku
		5. Na kraju uspesnog izvrsavanja vraca OK kao povratnu vrednost
	*/
	Status createSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags);

	/*
		Isto kao createSegment, s tim sto izmedju koraka 4 i 5 upisuje prosledjeni sadrzaj u klastere stranica
		Sadrzaj je iste velicine kao i segment u koji se upisuje
	*/
	Status loadSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags, void* content);

	/*
		Ako ne pronadje segment adresnog prostora sa pocetnom prosledjenom adresom vraca TRAP
		Za svaku stranicu deskriptorom proverava da li je ona trenutno ucitana
		Ako jeste, dodatno oslobadja okvir, u suprotnom samo oslobadja klaster na particiji
		Deskriptor postavlja na 0 - oznacava u tabeli drugog reda da je stranica adresa slobodna
		U slucaju da je oslobodjen poslednji deskriptor u tabeli drugog reda, ona se oslobadja
	*/
	Status deleteSegment(VirtualAddress startAddress);

	/*
		KernelSystem alocira okvir, jer u slucaju zamene stranica radi se globalna zamena
		Prebacuje se sadrzaj klastera date stranice u alocirani okvir
		Azuriraju se broj okvira i bit V deskriptora stranice
	*/
	Status pageFault(VirtualAddress address);

	/*
		Vraca fizicku adresu koja pripada datoj logickoj adresi, ako je prosledjena adresa deo alociranog adresnog prostora
		U suprotnom vraca nullptr
	*/
	PhysicalAddress getPhysicalAddress(VirtualAddress address);
protected:
	friend class KernelSystem;
private:
	Process* pProcess;
	KernelSystem* pSystem;
	
	ProcessId pid;
	PhysicalAddress* pmt; // PMT prvog nivoa (pokazivac na okvir sa nizom adresa na tabele drugog nivoa)
	AddressSpaceManager adrSM;
private:
	/*
		Alocira klaster koji dodeljuje stranici i njegov broj upisuje u generisani deskriptor zajedno sa bitima pristupa
		Broj okvira kao i V i R bitovi ostaju 0
	*/
	PageDescriptor generatePageDescriptor(AccessType flags);
};

#endif