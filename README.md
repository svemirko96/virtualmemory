Implementation of virtual memory with paging for a university course teaching operating systems.

File of specified size is used as the secondary storage for storing swapped out pages.