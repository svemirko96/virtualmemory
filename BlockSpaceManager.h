#ifndef BLOCKSPACEMANAGER_H
#define BLOCKSPACEMANAGER_H

#include "vm_declarations.h"
#include <vector>

// using SpaceSegment = std::pair<PageNum, PageNum>;

/*
	Klasa sluzi za evidenciju slobodnih delova memorije izdeljene u vece celine - blokove.
*/

class BlockSpaceManager {
public:
	BlockSpaceManager(PhysicalAddress startAddress, PageNum size);

	PageNum allocateBlock();
	int deallocateBlock(PageNum block);
protected:
	friend std::ostream& operator<<(std::ostream& os, const BlockSpaceManager& bsm);
private:
	std::vector<std::pair<PageNum, PageNum>> freeBlocks;
	PageNum firstBlockNum; // number of first block
	PageNum size; // number of blocks
};

#endif