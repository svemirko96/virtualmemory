#ifndef ADDRESSSPACEMANAGER_H
#define ADDRESSSPACEMANAGER_H

#include <vector>
#include "vm_declarations.h"

class AddressSpaceManager {
public:
	static const PageNum VM_SPACE_SIZE;
public:
	using Segment = std::pair<VirtualAddress, PageNum>;

	AddressSpaceManager();
	// kreira novi segment sa datom pocetnom adresom i velicinom ako je to moguce
	int allocateSegment(VirtualAddress startAddress, PageNum segSize);
	// oslobadja adr. prostor koji segment zauzima ako postoji neki sa datom pocetnom adresom
	int deallocateSegment(VirtualAddress startAddress);
	PageNum getSegmentSize(VirtualAddress startAddress) const;
protected:
	friend class KernelProcess;
	friend std::ostream& operator<<(std::ostream& os, const AddressSpaceManager& adrSM);
private:
	// vodi evidenciju o svim alociranim segmentima
	std::vector<std::pair<VirtualAddress, PageNum>> segments;
};

#endif