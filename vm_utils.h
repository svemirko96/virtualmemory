#ifndef UTILS_H
#define UTILS_H

#include "vm_declarations.h"

PageNum getLvl1PmtEntry(VirtualAddress address);
PageNum getLvl2PmtEntry(VirtualAddress address);
PageNum getOffset(VirtualAddress address);

bool isFlagSet(PageDescriptor desc, PageDescFlag flag);
void setFlag(PageDescriptor* desc, PageDescFlag flag, bool val);

AccessType getAccesType(PageDescriptor desc);

PageNum getFrameNum(PageDescriptor desc);
void setFrameNum(PageDescriptor* pDesc, PageNum frameNum);
PageNum getClusterNum(PageDescriptor desc);
void setClusterNum(PageDescriptor* pDesc, PageNum clusterNum);

#endif