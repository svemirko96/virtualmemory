#ifndef KERNEL_SYSTEM_H
#define KERNEL_SYSTEM_H

#include "vm_declarations.h"
#include <vector>
#include <mutex>
#include "BlockSpaceManager.h"
#include "KernelProcess.h"

class System;
class Process;
class Partition;

class KernelSystem {
public:
	KernelSystem(System* system, PhysicalAddress processVMSpace, PageNum processVMSpaceSize, PhysicalAddress pmtSpace, PageNum pmtSpaceSize, Partition* partition);

	~KernelSystem();

	/*
		Kreira povezane objekte klasa Process i KernelProcess
		KernelProcess cuva pokazivac na KernelSystem koji ga je kreirao
		Alocira PMT prvog nivoa za proces i inicijalizuje njene ulaze sa nullptr
	*/
	Process* createProcess();

	Time periodicJob(); // nece biti korisceno

	/*
		
	*/
	Status access(ProcessId pid, VirtualAddress address, AccessType type); // HW
protected:
	friend class KernelProcess;
private:
	System* system;

	static ProcessId nextProcessID;
	std::vector<KernelProcess*> processes;

	// PMT space
	PhysicalAddress pmtSpace;
	PageNum pmtSpaceSize;
	BlockSpaceManager pmtSM;

	// Process VM space - frames for process pages
	PhysicalAddress processVMSpace;
	PageNum processVMSpaceSize;
	BlockSpaceManager pvmSM;

	// Swap partition
	Partition* partition;
	BlockSpaceManager partSM;

	// Pomocna struktura za pronalazenje sledece stranice za izbacivanje prilikom zamene
	// Cuva deskriptore trenutno ucitanih stranica
	std::vector<PageDescriptor*> loadedPageDescriptors;

	// Obezbedjuje atomicnost metoda kod kojih je to potrebno
	std::mutex mtx;
private:
	PageNum allocatePMTFrame();
	int deallocatePMTFrame(PageNum frameNum);
	PageNum allocateFrame(PageDescriptor* pDesc); // poziva je page-fault; po potrebi radi swap stranica
	int deallocateFrame(PageNum frameNum);
	PageNum allocateCluster();
	int deallocateCluster(PageNum clusterNum);

};

#endif